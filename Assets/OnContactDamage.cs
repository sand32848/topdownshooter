using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnContactDamage : MonoBehaviour
{
	[SerializeField] private UnityEvent onContactEvent;
	[SerializeField] private float damage = 20f;
	GameObject player => GameObject.FindWithTag("Player");
	PlayerHealth health => player.GetComponent<PlayerHealth>();

    public void doDamage()
	{
		health.reduceHealth(damage);
		onContactEvent.Invoke();
		Destroy(gameObject);
	}
}
