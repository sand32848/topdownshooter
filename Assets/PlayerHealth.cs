using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class PlayerHealth : MonoBehaviour
{
	[SerializeField] private UnityEvent onDeathEvent;
	[SerializeField] private float currentHealth;
	[SerializeField] public float maxHealth { get; set; } = 100;
	[SerializeField] private GameObject deathParticle;
	public static Action<float> onHealthChange;

	public void reduceHealth(float _health)
	{
		currentHealth -= _health;
		onHealthChange?.Invoke(currentHealth);

		if (currentHealth <= 0)
		{
			Death();
		}
	}

	public void Death()
	{
		spawnParticle();
		onDeathEvent?.Invoke();
		Destroy(gameObject);
	}

	public void spawnParticle()
	{
		if (deathParticle)
		{
			GameObject p = Instantiate(deathParticle, transform.position, Quaternion.identity);
			Destroy(p, 2);
		}
	}

	public void ChangeMaxHealth(float newHealth)
	{
		maxHealth = Mathf.Floor(newHealth);
	}
}
