using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class PauseManager : MonoBehaviour
{
	[SerializeField] private UnityEvent onPauseEvent;
	[SerializeField] private UnityEvent onUnpauseEvent;
	public static Action pauseSignal;
    private bool paused = false;
   

    // Update is called once per frame
    void Update()
    {
		if (InputController.Instance.ESC)
		{
			paused = !paused;

			if (paused)
			{
				Time.timeScale = 0;
				onPauseEvent.Invoke();

			}
			else
			{
				Time.timeScale = 1;
				onUnpauseEvent.Invoke();
			}
		}
    }

	public void PauseSwitch()
	{
		paused = false;
		Time.timeScale = 1;
		onUnpauseEvent.Invoke();
	}
}
