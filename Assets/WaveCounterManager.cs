using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WaveCounterManager : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI waveCounter;
    private int currentWave = 1;

	private void OnEnable()
	{
		WaveManager.onWaveEnd += UpdateWaveCount;
	}

	private void OnDisable()
	{
		WaveManager.onWaveEnd -= UpdateWaveCount;
	}

	private void UpdateWaveCount()
	{
		currentWave += 1;
		waveCounter.text = "Wave " + currentWave.ToString();
	}
}
