using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRateUpgrade : UpgradeAttribute
{
    PlayerShooting playerShooting => player.GetComponent<PlayerShooting>();
    public override void applyModifier()
	{
		increaseLevel();

		switch (currentLevel)
		{
			case 1:
				playerShooting.ChangeFireRate(playerShooting.shotPerSec * 1.1f);
				break;
			case 2:
				playerShooting.ChangeFireRate(playerShooting.shotPerSec * 1.1f);
				break;
			case 3:
				playerShooting.ChangeFireRate(playerShooting.shotPerSec * 1.1f);
				break;
			case 4:
				playerShooting.ChangeFireRate(playerShooting.shotPerSec * 1.1f);
				break;
		}
	}


}
