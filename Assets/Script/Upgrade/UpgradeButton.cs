using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class UpgradeButton : MonoBehaviour
{
    public static Action<UpgradeAttribute> onUpgrade;
    private UpgradeAttribute upgradeAttribute;
    [SerializeField] private TextMeshProUGUI upgradeName;
    [SerializeField] private TextMeshProUGUI upgradeDescription;
    [SerializeField] private TextMeshProUGUI upgradeLevel;

    public void addModifier()
	{
        onUpgrade?.Invoke(upgradeAttribute);
	}

    public void applyUpgradeInfo(UpgradeAttribute _upgradeAttribute)
	{
        upgradeAttribute = _upgradeAttribute;
        upgradeName.text = _upgradeAttribute.GetName();
        upgradeDescription.text = _upgradeAttribute.GetDescription();
        upgradeLevel.text = "LVL: " + _upgradeAttribute.GetLevel().ToString();
	}
}
