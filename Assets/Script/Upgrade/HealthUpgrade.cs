using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUpgrade : UpgradeAttribute
{
	PlayerHealth playerHealth => player.GetComponent<PlayerHealth>();
	public override void applyModifier()
	{
		increaseLevel();

		switch (currentLevel)
		{
			case 1:
				playerHealth.ChangeMaxHealth(playerHealth.maxHealth * 1.1f);
				break;
			case 2:
				playerHealth.ChangeMaxHealth(playerHealth.maxHealth * 1.1f);
				break;
			case 3:
				playerHealth.ChangeMaxHealth(playerHealth.maxHealth * 1.1f);
				break;
		}
	}


}
