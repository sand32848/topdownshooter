using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpeedUpgrade : UpgradeAttribute
{
	PlayerShooting playerShooting => player.GetComponent<PlayerShooting>();
	public override void applyModifier()
	{
		increaseLevel();

		switch (currentLevel)
		{
			case 1:
				playerShooting.ChangeProjectileSpeed(playerShooting.bulletSpeed * 1.1f);
				break;
			case 2:
				playerShooting.ChangeProjectileSpeed(playerShooting.bulletSpeed * 1.1f);
				break;
			case 3:
				playerShooting.ChangeProjectileSpeed(playerShooting.bulletSpeed * 1.1f);
				break;
		}
	}


}
