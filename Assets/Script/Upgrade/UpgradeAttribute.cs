using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UpgradeAttribute : MonoBehaviour
{
    [SerializeField] protected string upgradeName;
    [SerializeField] protected string upgradeDescription;
    [SerializeField] protected Image upgradeImage;
    [SerializeField] protected int currentLevel;
    [SerializeField] protected int maxLevel;
    protected GameObject player;

	public virtual void Start()
	{
        player = GameObject.FindWithTag("Player");
	}
	public abstract void applyModifier();

    public virtual void increaseLevel()
	{
        currentLevel += 1;
        currentLevel = Mathf.Clamp(currentLevel,0,maxLevel);
	}

    public string GetName()
	{
        return upgradeName;
	}

    public string GetDescription()
    {
        return upgradeDescription;
    }

    public int GetLevel()
	{
        return currentLevel;
	}

    public int GetMaxLevel()
	{
        return maxLevel;
	}

    public Image GetImage()
	{
        return upgradeImage;
	}
}
