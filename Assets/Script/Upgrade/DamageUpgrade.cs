using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpgrade : UpgradeAttribute
{
	PlayerShooting playerShooting => player.GetComponent<PlayerShooting>();
	public override void applyModifier()
	{
		increaseLevel();

		switch (currentLevel)
		{
			case 1:
				playerShooting.ChangeShootDamage(playerShooting.baseDamage * 1.1f);
				break;
			case 2:
				playerShooting.ChangeShootDamage(playerShooting.baseDamage * 1.1f);
				break;
			case 3:
				playerShooting.ChangeShootDamage(playerShooting.baseDamage * 1.1f);
				break;
			case 4:
				playerShooting.ChangeShootDamage(playerShooting.baseDamage * 1.1f);
				break;
		}

	}


}
