using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
	[SerializeField] private GameObject upgradePanel;
	[SerializeField] private GameObject upgradeBank;
	[SerializeField] private List<GameObject> upgradePanelList = new List<GameObject>();

	private List<UpgradeAttribute> upgradeList = new List<UpgradeAttribute>();
	private List<UpgradeAttribute> upgradeToRemove = new List<UpgradeAttribute>();
	private List<int> upgradeIndex = new List<int>();

	private void OnEnable()
	{
		WaveManager.onWaveEnd += triggerUpgrade;
	}

	private void OnDisable()
	{
		WaveManager.onWaveEnd -= triggerUpgrade;
	}

	private void Start()
	{
		Component[] upgradeInfo;

		upgradeInfo = upgradeBank.transform.GetComponents(typeof(UpgradeAttribute));

		for(int i = 0; i < upgradeInfo.Length; i++)
		{
			upgradeList.Add(upgradeInfo[i]  as UpgradeAttribute);
		}
	}

	public void triggerUpgrade()
	{
		removeMaxUpgrade();

		upgradePanel.SetActive(true);

		int[] upgradeIndex = new int[upgradeList.Count];

		for (int i = 0; i < upgradeList.Count; i++)
		{
			upgradeIndex[i] = i;
		}

		for (int i = 0; i < (upgradeIndex.Length); i++)
		{
			int r = i + (int)(Random.Range(0.0f, 1.0f) * (upgradeIndex.Length - i));
			int t = upgradeIndex[r];
			upgradeIndex[r] = upgradeIndex[i];
			upgradeIndex[i] = t;
		}

		if(upgradeList.Count < upgradePanelList.Count)
		{
			for (int i = 0; i < upgradePanelList.Count; i++)
			{
				upgradePanelList[i].SetActive(false);
			}

			for (int i = 0; i < upgradeList.Count; i++)
			{
				upgradePanelList[i].SetActive(true);
				upgradePanelList[i].GetComponent<UpgradeButton>().applyUpgradeInfo(upgradeList[upgradeIndex[i]]);
			}
		}
		else
		{
			for (int i = 0; i < upgradePanelList.Count; i++)
			{
				upgradePanelList[i].SetActive(true);
				upgradePanelList[i].GetComponent<UpgradeButton>().applyUpgradeInfo(upgradeList[upgradeIndex[i]]);
			}
		}
	}

	private void removeMaxUpgrade()
	{
		for(int i = 0; i < upgradeList.Count; i++)
		{
			if(upgradeList[i].GetMaxLevel() == upgradeList[i].GetLevel())
			{
				upgradeToRemove.Add(upgradeList[i]);
			}
		}

		for(int i = 0; i < upgradeToRemove.Count; i++)
		{
			upgradeList.Remove(upgradeToRemove[i]);
		}
	}
}
