using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UpgradeHolder", menuName ="Upgrade/UpgradeHolder", order = 1)]
public class UpgradeHolder : ScriptableObject
{
    [SerializeField] private UpgradeAttribute upgradeAttribute;

}
