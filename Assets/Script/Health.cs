using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Health : MonoBehaviour
{
	[SerializeField]  private UnityEvent onDeathEvent;
    [SerializeField] private float currentHealth;
	[SerializeField] private float maxHealth;
	[SerializeField] private GameObject deathParticle;

	public void reduceHealth(float _health)
	{
        currentHealth -= _health;

        if(currentHealth <= 0)
		{
			Death();
		}
	}

    public void Death()
	{
		spawnParticle();
		onDeathEvent?.Invoke();
		Destroy(gameObject);
	}

	public void spawnParticle()
	{
		if (deathParticle)
		{
			GameObject p = Instantiate(deathParticle, transform.position, Quaternion.identity);
			Destroy(p, 2);
		}
	}

}
