using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModifier : MonoBehaviour
{
    [SerializeField] private List<UpgradeAttribute> modifierList = new List<UpgradeAttribute>();
	private PlayerShooting playerShooting;
	private PlayerMovement playerMovement;
	private Health health;

	private void OnEnable()
	{
		UpgradeButton.onUpgrade += addModifier;
	}

	private void OnDisable()
	{
		UpgradeButton.onUpgrade -= addModifier;
	}

	private void Start()
	{
		playerShooting = GetComponent<PlayerShooting>();
		playerMovement = GetComponent<PlayerMovement>();
		health = GetComponent<Health>();
	}

	public void addModifier(UpgradeAttribute upgradeAttribute)
	{
		if (modifierList.Contains(upgradeAttribute))
		{
			UpgradeAttribute _upgradeAttribute = modifierList.Find(x => x == upgradeAttribute);
			_upgradeAttribute.applyModifier();
		}
		else
		{
			modifierList.Add(upgradeAttribute);
			upgradeAttribute.applyModifier();
		}
	}
}
