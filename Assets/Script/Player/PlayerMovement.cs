using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    public float speed { get; set; } = 12f;

	private void Awake()
	{
        controller = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        Vector3 move = new Vector3(InputController.Instance.movement.x,0, InputController.Instance.movement.y) ;

        controller.Move(move * speed *Time.deltaTime);
    }

    private void ResetMovementStat()
	{
        speed = 12;
	}

    public void ChangeMoveSpeed(float newSpeed)
	{
        speed = newSpeed;
	}
}
