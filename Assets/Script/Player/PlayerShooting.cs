using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    [SerializeField] private GameObject Bullet;
    [SerializeField] private LayerMask groundLayer;
    public float shotPerSec { get; set; } = 1;
    public float bulletSpeed { get; set; } = 20;
    public float baseDamage { get; set; } = 1;
    private float _shotCooldown;
    private float shotCooldown;
    private Camera camera;
    private Vector3 force;
    // Start is called before the first frame update
    void Start()
    {
        shotCooldown = 1 / shotPerSec;
        _shotCooldown = shotCooldown;
        camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        Shooting();
    }

    private void Shooting()
    {
        shotCooldown -= Time.deltaTime;
        if(shotCooldown > 0)
		{
            return;
		}

        Ray ray = Camera.main.ScreenPointToRay(InputController.Instance.mouseInput);
        RaycastHit rayInfo;

        if (Physics.Raycast(ray, out rayInfo, 300))
		{
            force = (new Vector3(rayInfo.point.x,transform.position.y,rayInfo.point.z) - transform.position).normalized;
            print(rayInfo.point);
        }

        GameObject b = Instantiate(Bullet, transform.position, Quaternion.identity);
        b.GetComponent<Rigidbody>().AddForce(force  * bulletSpeed,ForceMode.Impulse);
        b.GetComponent<Bullet>().setDamage(baseDamage);
        shotCooldown = _shotCooldown;

        Destroy(b, 5);
	}

    public void ResetShootStat()
	{
        baseDamage = 1;
        shotPerSec = 1;
        bulletSpeed = 20;
	}

    public void ChangeShootDamage(float newDamage)
	{
        baseDamage = newDamage;
	}

    public void ChangeFireRate(float newFirerate)
	{
        shotPerSec = newFirerate;
	}

    public void ChangeProjectileSpeed(float newSpeed)
	{
        bulletSpeed = newSpeed;
	}
}
