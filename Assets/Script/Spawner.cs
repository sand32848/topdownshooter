using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float spawnTimer;
    [SerializeField] private LayerMask layer;
    private List<GameObject> needToSpawn = new List<GameObject>();

    private bool allowMobSpawn = false;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

	private void Update()
	{
        spawnMob();
	}

	public float randomBetween(float min, float max)
	{
        float i = Random.Range(min, max);
        return i;
	}

    public void spawnMob()
	{
        if(needToSpawn.Count == 0)
		{
            return;
		}

        int result = Random.Range(0, 4);
        float xRange;
        float yRange;

        if (result == 0)//upper
        {
            xRange = randomBetween(-2f, 2f);
            yRange = randomBetween(1.5f, 2f);
        }
        else if (result == 1)//down
        {
            xRange = randomBetween(-2f, 2f);
            yRange = randomBetween(-0.5f, -1f);
        }

		else if (result == 2)//right
		{
            xRange = randomBetween(1.5f, 2f);
            yRange = randomBetween(-1.5f, 2f);
		}
		else //left
		{
            xRange = randomBetween(-0.5f, -1f);
            yRange = randomBetween(-1.5f, 2f);
		}

        Vector3 v3Pos = Camera.main.ViewportToWorldPoint(new Vector3(xRange, yRange, 14f));
        RaycastHit hit;

        if (Physics.Raycast(v3Pos, Vector3.down, out hit, Mathf.Infinity, layer))
        {
            int i = randomFromList();
            Instantiate(needToSpawn[i], hit.point, Quaternion.identity);
            needToSpawn.RemoveAt(i);
        }
    }

    public void AddToSpawnList(GameObject mob)
	{
        needToSpawn.Add(mob);
	}

    public int randomFromList()
	{
        if(needToSpawn.Count == 0)
		{
            return 0;
		}

        int random = Random.Range(0, needToSpawn.Count);
        return random;
	}
}
